<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="/CSS/style.css">
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top"> KelasProgramming.com
            </a>
        </div>
    </nav>
    <div class="hero-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>KelasProgramming.com</h1>
                    <p>Kelas PHP secara online dan sambilan</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">

        <div class="row">
            <div class="col text-center">
                <h3>Gambar-gambar Pekan</h3>
            </div>
        </div>
        <div class="row">

        <?php foreach($all_pekan as $pekan) :?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-3">
                <div class="card">
                    <img src="<?php echo $pekan ['gambar']?> " class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5><?php echo $pekan ['nama'];?></h5>
                        <p class="card-text"><?php echo $pekan ['description'];?> </p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

           
        <!-- /row -->

        <div class="row p-5">
            <div class="col-12 text-center">

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <footer class="text-center p-5">
        <p>Hakcipta terpelihara &copy; 2021</p>
    </footer>
</body>

</html>